<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Baum\Node;

/**
* TREE
*/
class Category extends Node {

  /**
   * Table name.
   *
   * @var string
   */
   protected $table = "categories"; //название таблицы в базе
   protected $fillable = ['id','parent_id', 'rgt', 'lft', 'depth', 'preview','name'];

  public function products()
  {
    return $this->belongsToMany('App\Products', 'product_category', 'category_id', 'product_id');
  }

  public static function boot()
  {
    parent::boot();

    self::deleting(function($category)
    {
      $categories_id = $category->descendants()->pluck('id');
      $categories_id[] = $category->getKey();
      $products = Products::whereHas('categories', function($q) use ($categories_id) {
          $q->whereIn('categories.id', $categories_id);
        })->get();

      if($products->count() > 0)
      {
        $html = "Невозможно удалить эту категорию так как она содержит следующие товары:<br>\n";
        $html .= "<ul>\n";
        foreach ($products as $product) {
          $html .= "    <li>$product->title</li>\n";
        }
        $html .= "</ul>\n";
        Category::rebuild();        

        \Session::flash('errorMsg', $html);

        return false;
      }      
    });
  }

  public function scopeSearchByKeyword($query, $keyword)
  {
    if ($keyword!='') {
      $query->where(function ($query) use ($keyword) {
        $query->where("name", "LIKE","%$keyword%");
      });
    }
    return $query;
  }  

  //////////////////////////////////////////////////////////////////////////////

  //
  // Below come the default values for Baum's own Nested Set implementation
  // column names.
  //
  // You may uncomment and modify the following fields at your own will, provided
  // they match *exactly* those provided in the migration.
  //
  // If you don't plan on modifying any of these you can safely remove them.
  //

  // /**
  //  * Column name which stores reference to parent's node.
  //  *
  //  * @var string
  //  */
  // protected $parentColumn = 'parent_id';

  // /**
  //  * Column name for the left index.
  //  *
  //  * @var string
  //  */
  // protected $leftColumn = 'lft';

  // /**
  //  * Column name for the right index.
  //  *
  //  * @var string
  //  */
  // protected $rightColumn = 'rgt';

  // /**
  //  * Column name for the depth field.
  //  *
  //  * @var string
  //  */
  // protected $depthColumn = 'depth';

  // /**
  //  * Column to perform the default sorting
  //  *
  //  * @var string
  //  */
  // protected $orderColumn = null;

  // /**
  // * With Baum, all NestedSet-related fields are guarded from mass-assignment
  // * by default.
  // *
  // * @var array
  // */
  // protected $guarded = array('id', 'parent_id', 'lft', 'rgt', 'depth');

  //
  // This is to support "scoping" which may allow to have multiple nested
  // set trees in the same database table.
  //
  // You should provide here the column names which should restrict Nested
  // Set queries. f.ex: company_id, etc.
  //

  // /**
  //  * Columns which restrict what we consider our Nested Set list
  //  *
  //  * @var array
  //  */
  // protected $scoped = array();

  //////////////////////////////////////////////////////////////////////////////

  //
  // Baum makes available two model events to application developers:
  //
  // 1. `moving`: fired *before* the a node movement operation is performed.
  //
  // 2. `moved`: fired *after* a node movement operation has been performed.
  //
  // In the same way as Eloquent's model events, returning false from the
  // `moving` event handler will halt the operation.
  //
  // Please refer the Laravel documentation for further instructions on how
  // to hook your own callbacks/observers into this events:
  // http://laravel.com/docs/5.0/eloquent#model-events

}
