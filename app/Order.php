<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
   protected $table = "orders"; //название таблицы в базе
   protected $fillable = ['full_name', 'email', 'phone', 'additional_info', 'product_id', 'viewed'];

	public function product() 
	{
		return $this->belongsTo('App\Products');
	}

}
