<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Products;
use App\Category;

class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCategories()
    {       
        $categoriesRoot = Category::roots()->get();
        $forKeywords = "";
        foreach($categoriesRoot as $category){
            if (iconv_strlen($forKeywords) < 255 and (iconv_strlen($forKeywords) + iconv_strlen("$category->name,")) < 255) {
                $forKeywords .= "$category->name,";

            } else{
                break;
            }
        }
        $tree = "";
        $categories = ProductsController::SidebarDropdowns($categoriesRoot,$tree); 
        $tree = "";
        $categoriesRecursive = FrontController::recursiveArrayCategoriesTwo($categoriesRoot,$tree); 
        return view('layouts.category', compact('categories', 'categoriesRecursive', 'forKeywords'));
    }

    public function showProductsOfCategory($id)
    {
        $category = Category::findOrFail($id);
        $categoriesRoot = Category::roots()->get();     
        $tree = "";
        $categories = ProductsController::SidebarDropdowns($categoriesRoot,$tree); 
        $tree = "";
        $descendants = $category->children()->get();
        $categoriesRecursive = FrontController::recursiveArrayCategories($descendants,$tree);
        if(!$category->isLeaf()){
            $immediateDescendants = $category->getImmediateDescendants();
            $forKeywords = "$category->name,";
                foreach($immediateDescendants as $immdtAndSelf){
                    if (iconv_strlen($forKeywords) < 255 and (iconv_strlen($forKeywords) + iconv_strlen("$immdtAndSelf->name,")) < 255) {
                        $forKeywords .= "$immdtAndSelf->name,";

                    } else{
                        break;
                    }
                }
        } else{
        $ancestorsAndSelf = $category->getAncestorsAndSelf();
            $forKeywords = "";
                foreach($ancestorsAndSelf as $ancstrsAndSelf){
                    if (iconv_strlen($forKeywords) < 255 and (iconv_strlen($forKeywords) + iconv_strlen("$ancstrsAndSelf->name,")) < 255) {
                        $forKeywords .= "$ancstrsAndSelf->name,";

                    } else{
                        break;
                    }
                }        
        }
        $products = $category->products()->paginate(3);
        return view('layouts.products', compact('category', 'categoriesRecursive', 'categories', 'forKeywords', 'products'));
    }

    public function showProduct($id)
    {
        $products = Products::find($id); // получаем все, что касается товара (название, цена....)
        $parameters = Products::parameters($id);//получаем все параметры
        $images = explode(';',$products->preview); //ссылки на картинки передаем отдельным массивом
        $categoriesRoot = Category::roots()->get();     
        $tree = "";
        $categories = ProductsController::SidebarDropdowns($categoriesRoot,$tree); 
        $forKeywords = "$products->title,";
        foreach ($products->categories as $value) {
            if (iconv_strlen($forKeywords) < 255 and (iconv_strlen($forKeywords) + iconv_strlen("$value->name,")) < 255) {
                $forKeywords .= "$value->name,";
            } else{
                break;
            }            
        }
        return view('layouts.show', compact('products', 'parameters', 'images', 'categories', 'id', 'forKeywords'));        
    }

    function recursiveArrayCategories($array, &$tree)
    {
        $tree .= "<ul>";    
        foreach($array as $value){            
            $tree .= "<li>";
            if ($value->isRoot()) {
                $tree .= "<a href=\"/categories/".$value->id."\">";
                    $tree .= "<h2>".$value->name."</h2>";
                $tree .= "</a>";
            } elseif($value->getLevel() == 1) {
                $tree .= "<a href=\"/categories/".$value->id."\">";
                    $tree .= "<h3>".$value->name."</h3>";
                $tree .= "</a>";
            } elseif($value->getLevel() == 2) {
                $tree .= "<a href=\"/categories/".$value->id."\">";
                    $tree .= "<h4>".$value->name."</h4>";
                $tree .= "</a>";
            } elseif($value->getLevel() == 3) {
                $tree .= "<a href=\"/categories/".$value->id."\">";
                    $tree .= "<h5>".$value->name."</h5>";
                $tree .= "</a>";
            } elseif($value->getLevel() > 3) {
                $tree .= "<a href=\"/categories/".$value->id."\">";
                    $tree .= "<h6>".$value->name."</h6>";
                $tree .= "</a>";
            }        
            if($value->children()->count() > null){
                FrontController::recursiveArrayCategories($value->children()->get(),$tree);
            }
            $tree .= "</li>";
        }
        $tree .= "</ul>";        
        return $tree;
    }

    function recursiveArrayCategoriesTwo($array, &$tree)
    {
        foreach($array as $value){
            if ($value->isRoot()){
                $tree .= "<div class=\"col-md-4\">";
                    $tree .= "<ul>";            
                        $tree .= "<li>";                
            } else {
                    $tree .= "<ul>";            
                        $tree .= "<li>";                
            }
            if ($value->isRoot()) {
                $tree .= "<a href=\"/categories/".$value->id."\">";
                    $tree .= "<h2>".$value->name."</h2>";
                $tree .= "</a>";
            } elseif($value->getLevel() == 1) {
                $tree .= "<a href=\"/categories/".$value->id."\">";
                    $tree .= "<h3 style=\"color:red\">".$value->name."</h3>";
                $tree .= "</a>";
            } elseif($value->getLevel() == 2) {
                $tree .= "<a href=\"/categories/".$value->id."\">";
                    $tree .= "<h4>".$value->name."</h4>";
                $tree .= "</a>";
            } elseif($value->getLevel() == 3) {
                $tree .= "<a href=\"/categories/".$value->id."\">";
                    $tree .= "<h5>".$value->name."</h5>";
                $tree .= "</a>";
            } elseif($value->getLevel() > 3) {
                $tree .= "<a href=\"/categories/".$value->id."\">";
                    $tree .= "<h6>".$value->name."</h6>";
                $tree .= "</a>";
            }     
            if($value->children()->count() > null){
                FrontController::recursiveArrayCategories($value->children()->get(),$tree);
            }
            if ($value->isRoot()){
                    $tree .= "</li>";
                $tree .= "</ul>";
            $tree .= "</div>";               
            } else {
                $tree .= "</li>";
            $tree .= "</ul>";
            }                        
        }
        return $tree;
    }

    public function search(Request $request)
    {
        $keyword = $request->keyword;
        $categoriesInSearch = Category::SearchByKeyword($keyword)->paginate(10);
        $productsInSearch = Products::SearchByKeyword($keyword)->paginate(6);
        $categoriesRoot = Category::roots()->get();
        $tree = "";
        $categories = ProductsController::SidebarDropdowns($categoriesRoot,$tree);       
        return view('layouts.search', compact('categoriesInSearch','productsInSearch','categories','keyword'));
    }

    public function showContacts()
    {
        $categoriesRoot = Category::roots()->get();
        $tree = "";
        $categories = ProductsController::SidebarDropdowns($categoriesRoot,$tree); 
        return view('layouts.contacts', compact('categories'));
    }

    public function showDocuments()
    {
        $categoriesRoot = Category::roots()->get();
        $tree = "";
        $categories = ProductsController::SidebarDropdowns($categoriesRoot,$tree); 
        return view('layouts.documents', compact('categories'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}