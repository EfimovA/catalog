<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class HelpController extends Controller
{
	function recursiveArrayTable($array)
	{
		foreach ($array as $value) {
			$totalChild = count($value->descendants());
			$nbsp = "";
			for ($i=0; $i < $value->depth; $i++) {
				$nbsp .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			}

			echo "<tr>\n";
			echo "	<td>$nbsp\n";

			if($totalChild > 0)
			{
				echo "		<i class=\"fa fa-caret-down\"></i>&nbsp;&nbsp;\n";
			} else {
				echo "		<i class=\"fa fa-caret-right\"></i>&nbsp;&nbsp;\n";
			}

			echo "		$value->title\n";
			echo "	</td>\n";

			echo "	<td class=\"text-center\">\n";
			echo "		<a href=\"".route('categories.edit', ['categories' => $value->id])."\" type=\"button\" class=\"btn btn-xs btn-warning\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\">\n";
			echo "			<span class=\"glyphicon glyphicon-edit\" aria-hidden=\"true\"></span>\n";
			echo "		</a>";
			echo "		<a href=\"".route('categories.destroy', ['categories' => $value->id])."\" class=\"btn btn-xs btn-danger\" data-delete=\"".csrf_token()."\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\">\n";
			echo "			<span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span>\n";
			echo "		</a>\n";
			echo "	</td>\n";

			echo "<tr>\n";

			recursiveArrayTable($value->descendants());
		}
	}
}
