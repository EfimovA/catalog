<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Products;
use App\Category;
use App\Parameters_values;
use App\Parameters;
use Validator;


class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $this->authorize('index'); // <---- вот это важная строчка
        if($request->has('filter'))
        {
            $filter = $request->input('filter');
            $products = Products::whereHas('categories', function($q) use ($filter) {
                                    $q->whereIn('categories.id', $filter);
                                })->simplePaginate(10);
        } else {
            $filter = "";
            $products = Products::with('categories')->simplePaginate(10);
        }

        $categories = Category::all();
        $options = '';
        $options = ProductsController::recursiveArraySelect2($categories, $filter, $options);
        return view('admin.products.index', compact('categories', 'products', 'filter', 'options'));
    }

    public function indexFront()
    {
        $categories = Category::roots()->get();
        $tree = "";
        $categories = ProductsController::SidebarDropdowns($categories,$tree);
        $parameters = parameters::all();//получаем все параметры
        return view('layouts.index', ['parameters'=>$parameters, 'categories'=>$categories]);
    }

    static function ArrayDropdowns($array,$tree){
        foreach ($array as $value) {            
            if($value->isRoot()){
                $totalChild = $value->descendants()->count();
                $tree .= "<div class=\"btn-group dropdown\">\n";
                if($totalChild > 0){
                    $tree .= "<a id=\"dLabel\" role=\"button\" data-toggle=\"dropdown\" class=\"btn btn-default btn-drop\" data-target=\"#\" href=\"/categories/".$value->id."\">".$value->name." <span class=\"caret\"></span></a>\n";
                } else{
                    $tree .= "<a id=\"dLabel\" role=\"button\" data-toggle=\"dropdown\" class=\"btn btn-default  btn-drop\" data-target=\"#\" href=\"/categories/".$value->id."\">".$value->name."</a>\n";
                }
                if($totalChild > 0)
                {
                    $tree .= "<ul class=\"dropdown-menu multi-level\" role=\"menu\" aria-labelledby=\"dropdownMenu\">\n";
                    ProductsController::recursiveChildrensDropdowns($value->children()->get(),$tree);
                    $tree .= "</ul>\n";
                }
                $tree .= "</div>\n";
            }
        }
        return $tree;
    }

    static function recursiveChildrensDropdowns($children,&$tree){        
        foreach ($children as $value) { 
            if($value->descendants()->count() > 0){
                $tree .= "<li class=\"dropdown-submenu\">\n";
                $tree .= "<a tabindex=\"-1\" href=\"/categories/".$value->id."\">".$value->name."</a>\n";
                    $tree .= "<ul class=\"dropdown-menu\">\n";
                    ProductsController::recursiveChildrensDropdowns($value->children()->get(),$tree);
                    $tree .= "</ul>\n";
                $tree .= "</li>\n";
            } else{
                $tree .= "<li><a href=\"/categories/".$value->id."\">".$value->name."</a></li>\n";
            }
        }        
    }

    static function SidebarDropdowns($array,&$tree)
    {
      foreach ($array as $value) {
        $totalChild = count($value->children);
        if ($value->isRoot() and $totalChild == 0){
          $tree.= "<li>\n";
            $tree.= "<a href=\"/categories/".$value->id."\">".$value->name."</a>\n";
          $tree.= "</li>\n";
        } elseif ($value->isRoot() and $totalChild > 0){
          $tree.= "<li>\n";
            $tree.= "<a href=\"#\">".$value->name."<span class=\"fa arrow\"></span></a>\n";
                $tree.= "<ul class=\"nav nav-second-level\">\n"; 
                  ProductsController::SidebarDropdowns($value->children()->get(),$tree);
                $tree.= "</ul>\n";
          $tree.= "</li>\n";          
        }
        if($value->getLevel() > 0){
                $tree.= "<li>\n";
                  if(count($value->children) == 0){
                    $tree.= "<a href=\"/categories/".$value->id."\">".$value->name."</a>\n";
                  } else{
                    $tree.= "<a href=\"#\">".$value->name."<span class=\"fa arrow\"></span></a>\n";
                    if($value->getLevel() == 2){
                        $tree.= "<ul class=\"nav nav-third-level\">\n";
                    } else{
                        $tree.= "<ul class=\"nav nav-third-level\">\n";
                    }                    
                    ProductsController::SidebarDropdowns($value->children()->get(),$tree);
                    $tree.= "</ul>\n";
                  }                
                $tree.= "</li>\n";
        }
      }
      return $tree;
    } 

    function recursiveArraySelect2($array, $ids = null, $options)
    {
        foreach ($array as $value) {
            $totalChild = count($value->children);

            if(is_array($ids))
            {
                $ids = $ids;
            } else {
                $ids = array($ids);
            }

            if ($ids != null && in_array($value->id, $ids))
            {
                $options .= "<option value=\"$value->id\" selected>$value->name</option>";
            } else {
                $options .= "<option value=\"$value->id\">$value->name</option>";
            }

            ProductsController::recursiveArraySelect2($value->children, $ids, $options);
        }
        return $options;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $this->authorize('create'); // <---- вот это важная строчка
        $categories = Category::getNestedList('name',null,'&nbsp;&nbsp;&nbsp;&nbsp;'); //выбираем все категории
        $parameters=Parameters::all();
        return view('admin.products.create', compact('categories','parameters'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
            'category_id' => 'required',
            'meta_description' => 'required|max:255',
            'preview' => 'required',
            ]); 
        if(!$validator->fails())
        {
        $path = public_path()."/images/"; //определяем папку для сохранения картинок

                        //Сохраняем картинки
        $url_img=[];

        var_dump($request->file('preview'));
                     // массив, который будет содержать ссылки на все картинки
        if (is_array($request->file('preview'))){
            foreach($request->file('preview') as $file) //обрабатываем массив с файлами
            {

                if(empty($file)) continue;
                // если <input type="file"... есть, но туда ничего не загруженно, то пропускаем
                $extension = $file->getClientOriginalExtension();
                $filename = ProductsController::getRandomFileName($path, $extension);

                $url_img[]='/images/'.$filename.'.'.$extension; //добавляем url картинки в массив
                $file->move($path,$filename.'.'.$extension); //перемещаем файл в папку
            }
        }

        $preview=implode(';',$url_img); //массив с ссылками переводим в строку, что бы сохранить в базу.
        //Сохраняем каждый параметр
        $product=new Products;
        $product->title=$request->title; //название
        $product->description=$request->description;//описание
        $product->preview=$preview; //ссылки на картинки        
        $product->public=$request->public;
        $product->meta_description=$request->meta_description; // цена
        $product->meta_keywords=$request->meta_keywords; //ссылки на картинки
        $categories_id = $request->category_id;
        // $product->categories()->attach($categories_id);
        $product->save(); // Сохраняем все в базу.
        $product->categories()->sync($categories_id, false);
        //Обратабываем массивы с параметрами и их значениями.
        if (is_array($request->parameter)){
            $out=array_combine($request->parameter,$request->value);
        } // массив будет такой ['5'=>'300'], 5 - это id параметра, 300 - значение параметра
        //Сохраняем все параметры и значения в базу
        if(empty($out))
        {
            return back()->with('message','Товар сохранен');
        } //если нет ни одного параметра то просто редиректим обратно.

        foreach($out as $param=>$value)
        {
            $parameters=new Parameters_values;
            $parameters->parameters_id=$param;
            $parameters->products_id=$product->id;
            $parameters->value=$value;
            $parameters->save();
        }
        return back()->with('message','Товар сохранен');
    } else {
        $errorMsg = $validator->messages();
        return redirect()
        ->back()
        ->withErrors($errorMsg)
        ->withInput();
    }
}

    public static function getRandomFileName($path, $extension='')
    {
        $extension = $extension ? '.' . $extension : '';
        $path = $path ? $path . '/' : '';
 
        do {
            $name = md5(microtime() . rand(0, 9999));
            $file = $path . $name . $extension;
        } while (file_exists($file));
 
        return $name;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('edit'); // <---- вот это важная строчка
        $product=Products::find($id); //основные параметры
        $parameters=Products::parameters($id); 
        $categories = Category::getNestedList('name',null,'&nbsp;&nbsp;&nbsp;&nbsp;'); 
    //дополнительные параметры товара
        $parameters_all=Parameters::all(); //все параметры в базе
        if(strlen($product->preview)>0) //проверяем, есть ли изображения в базе
        {
            $images=explode(';',$product->preview);//изображения товара
        }
        else
        {
            $images=[];
        }
        return view('admin.products.edit',['product'=>$product,'parameters'=>$parameters,'images'=>$images,'parameters_all'=>$parameters_all, 'categories'=>$categories]);
    }

    public function del_image(Request $request)
    {
        $src = $request->input("src");
        $product_id = $request->input("product_id");
        $product = Products::find($product_id);
        $images = explode(";", $product->preview);//преобразуем строку в массив
        $root = public_path(); //путь до картинок
        if(($key = array_search($src,$images)) >= 0) //находим ключ, значение, которого соответствует ссылке на картинку
        {
            unset($images[$key]); //удалем ссылку из массива
            if(file_exists($root.$src)) //проверяем существование файла
            {
                unlink($root.$src); //удаляем файл
            }
        }
        $url = implode(";", $images); //переделываем массив строку
        $product->preview = $url; //обновляем значение в поле preview
        $product->save(); //сохраняем изменения
        return "OK";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product=Products::find($id); 
        if($product->preview == null){       
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
            'preview' => 'required',
            'category_id' => 'required',
            'meta_description' => 'required|max:255',
            ]);
    } else {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
            'category_id' => 'required',
            'meta_description' => 'required|max:255',
            ]);
    }
        if(!$validator->fails())
        { 
         $path=public_path()."/images/"; //определяем папку для сохранения картинок
         //Сохраняем картинки
         $url_img=[]; // массив, который будет содержать ссылки на все картинки
         if (is_array($request->file('preview'))){
             foreach($request->file('preview') as $file) //обрабатываем массив с файлами
             {
                  if(empty($file)) continue; // если <input type="file"... есть, но туда ничего не загруженно, то пропускаем

                  $extension = $file->getClientOriginalExtension();
                  $filename = ProductsController::getRandomFileName($path, $extension);
                  $url_img[]='/images/'.$filename.'.'.$extension; //добавляем url картинки в массив
                  $file->move($path,$filename.'.'.$extension); //перемещаем файл в папку
              }
         }

         // Сохраняем товар
         $product=Products::find($id);
         $product->title=$request->title; //название
         $product->description=$request->description;//описание
         $product->public=$request->public;
         $product->meta_description=$request->meta_description; // цена
         $product->meta_keywords=$request->meta_keywords; //ссылки на картинки
         $categories_id = $request->category_id;


         strlen($product->preview) ? $product->preview=explode(';',$product->preview) : $product->preview=[]; // если в базе нет ссылок, то возвращаем пустой массив либо поулчаем массив из строк
         $product->preview=implode(';',array_merge($product->preview,$url_img));//создаем строку с ссылками;  //ссылки на картинки, добавляем ссылки к существующей строке
         $product->save(); // Сохраняем все в базу.
         $product->categories()->detach();
         $product->categories()->sync($categories_id, false);
         if(empty($request->parameter)){
            Products::del($id);
         }
    //Обратабываем массивы с параметрами и их значениями.
        if (is_array($request->parameter)){
             $out=array_combine($request->parameter,$request->value); // массив будет такой ['5'=>'300'], 5 - это id параметра, 300 - значение параметра
             //Удаляем все дополнительные параметры товара и их значения             
             Products::del($id);
             //Сохраняем все параметры и значения в базу
             foreach($out as $param=>$value)
             {
              $parameters=new Parameters_values;
              $parameters->parameters_id=$param;
              $parameters->products_id=$id;
              $parameters->value=$value;
              $parameters->save();
             }
        }
        return back()->with('message', "Товар изменен" );
        } else {
            $errorMsg = $validator->messages();
            return back()
            ->withErrors($errorMsg)
            ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Products::find($id);
        $product->delete();
        $root = public_path();
        if(!empty($product->preview))
        {
            unlink($root . $product->preview); //удаляем превьюшку
        }
        return back()->with('message','Продукция удалена');
    }
}
