<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index'); // <---- вот это важная строчка
        $categories = Category::all();
        $tree = '';
        $tree = CategoriesController::recursiveArrayTable($categories,$tree);
        return view('admin.categories.categories', compact('tree'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create'); // <---- вот это важная строчка        
        $categories=Category::getNestedList('name',null,'&nbsp;&nbsp;&nbsp;&nbsp;');
        return view('admin.categories.create', compact('categories'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);   
        $path = public_path()."/images/"; //определяем папку для сохранения картинок

                //Сохраняем картинки
        $url_img=[];

        var_dump($request->file('preview'));
             // массив, который будет содержать ссылки на все картинки
        if (is_array($request->file('preview'))){
                    foreach($request->file('preview') as $file) //обрабатываем массив с файлами
                    {

                        if(empty($file)) continue;
                        // если <input type="file"... есть, но туда ничего не загруженно, то пропускаем
                        $extension = $file->getClientOriginalExtension();
                        $filename = CategoriesController::getRandomFileName($path, $extension);

                        $url_img[]='/images/'.$filename; //добавляем url картинки в массив
                        $file->move($path,$filename); //перемещаем файл в папку
                    }
                }

                $preview=implode(';',$url_img); //массив с ссылками переводим в строку, что бы сохранить в базу.
                if ($request->parent_id == 'null') {
                    $root = new Category;
                    $root->name = $request->name;
                    $root->preview = $preview;
                    $root->save();
                } else {
                    $parent = Category::find($request->parent_id);
                    $child1   = new Category;
                    $child1->name = $request->name;
                    $child1->preview = $preview;
                    $child1->save();                
                    $child1->makeChildof($parent);
                }
                return back()->with('message', 'Категория добавлена');
            }


    public static function getRandomFileName($path, $extension='')
    {
        $extension = $extension ? '.' . $extension : '';
        $path = $path ? $path . '/' : '';
 
        do {
            $name = md5(microtime() . rand(0, 9999));
            $file = $path . $name . $extension;
        } while (file_exists($file));
 
        return $name;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('edit'); // <---- вот это важная строчка
        $category = Category::find($id);
        return view('admin.categories.edit', ['category' => $category]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);
        $category = Category::find($id);
        $category->update($request->all());
        $category->save();
        return back()->with('message', 'Категория обновлена');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findorFail($id);        
        if ($category->descendants()->count() > 0)
        {
        $html = "Эта категория содержит подкатегории, вы не можете удалить эту категорию, пока не удалите все подкатегории";

        return redirect()
            ->back()
            ->with('errorMsg', $html);
        }
        if(!$category->delete())
        {
            return redirect('adminzone/categories');
        }
        return redirect()->back()->with('message', 'Категория ' .$category->name. ' удалена');
        //return $category->title; // метод возвратит название удаленной категории, которую мы отобразим в окне alert
    }

    public function build_tree($cats,$parent_id,$only_parent = false){
        if(is_array($cats) and isset($cats[$parent_id])){
            $tree = '<tr>';
            if($only_parent==false){
                foreach($cats[$parent_id] as $cat){
                    $tree .= '<td>'.$cat['id'].'</td>';
                    $tree .= '<td>'.' #'.$cat['name']. '</td>';
                    $tree .= "<td><a href=\"".action('CategoriesController@edit',$cat['id'])."\">Изменить</a></td>";
                    $tree .= "<td><form method=\"POST\" action=\"".action('CategoriesController@destroy',[$cat['id']])."\">
                    <input type=\"hidden\" name=\"_method\" value=\"delete\">
                    <input type=\"hidden\" name=\"_token\" value=\"".csrf_token()."\">
                    <input type=\"submit\" value=\"Удалить\">
                </form></td>";
                $tree .=  CategoriesController::build_tree($cats,$cat['id']);
            }
        }elseif(is_numeric($only_parent)){
            $cat = $cats[$parent_id][$only_parent];
            $tree .= '<td>'.$cat['id'].'</td>';
            $tree .= '<td>'.' #'.$cat['name']. '</td>';
            $tree .= "<td><a href=\"".action('CategoriesController@edit',$cat['id'])."\">Изменить</a></td>";
            $tree .= '<td><button><span class="del">Удалить</span></button></td>';
            $tree .=  CategoriesController::build_tree($cats,$cat['id']);
        }
        $tree .= '</tr>';
    }
    else return null;
    return $tree;
}

function recursiveArrayTable($array,$tree)
{
    foreach ($array as $value) {
        $totalChild = $value->descendants()->count();
        $nbsp = "";
        for ($i=0; $i < $value->getLevel(); $i++) {
            $nbsp .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        }


        $tree .=  "<tr>\n";
        $tree .=  "  <td>$nbsp\n";

        if($totalChild > 0)
        {
            $tree .=  "      <i class=\"glyphicon glyphicon-triangle-bottom\"></i>&nbsp;&nbsp;\n";
        } else {
            $tree .=  "      <i class=\"glyphicon glyphicon-triangle-right\"></i>&nbsp;&nbsp;\n";
        }

        $tree .=  "      $value->name\n";
        $tree .=  "  </td>\n";

        $tree .=  "  <td class=\"text-center\">\n";
        $tree .=  "      <a href=\"".route('categories.edit', ['categories' => $value->id])."\" type=\"button\" class=\"btn btn-xs btn-warning\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Изменить\">\n";
        $tree .=  "          <span class=\"glyphicon glyphicon-edit\" aria-hidden=\"true\"></span>\n";
        $tree .=  "      </a>";
        $tree .=  "      <a href=\"".route('categories.destroy', [$value->id])."\" class=\"btn btn-xs btn-danger\" data-delete=\"".csrf_token()."\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Удалить\">\n";
        $tree .=  "          <span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span>\n";
        $tree .=  "      </a>\n";
        $tree .=  "  </td>\n";

        $tree .=  "<tr>\n";

        CategoriesController::recursiveArrayTable($value->children,$tree);
    }
    return $tree;
}
}
