<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Products;
use App\Category;
use App\Order;
use Validator;
use DB;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index');
        $orders = Order::orderBy('viewed', 'asc')
                ->latest()
                ->get();
        return view('admin.orders.index', compact('orders'));
    }

    public function readOrder($id)
    {
        $this->authorize('read');
        $order = Order::findOrFail($id);        
        $order->viewed = true;
        $order->save();
        return view('admin.orders.read', compact('order'));
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $product = Products::findOrFail($id);
        $categoriesRoot = Category::roots()->get();
        $tree = "";
        $categories = ProductsController::SidebarDropdowns($categoriesRoot,$tree);        
        return view('layouts.orders', compact('product', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|max:255',
            'email' => 'required|email',
            'phone' => ['required', 'regex:/((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{6,10}/'],
            'additional_info' => 'required|max:255',
            // 'g-recaptcha-response' => 'required|captcha'
            ]); 
        if(!$validator->fails())
        {
            $order=new Order;
            $order->full_name=$request->full_name; //название
            $order->email=$request->email;//описание
            $order->phone=$request->phone; //ссылки на картинки        
            $order->additional_info=$request->additional_info;
            $order->product_id=$request->product_id;
            $order->product()->associate(Products::find($request->product_id));
            $order->viewed=false;
            $order->save(); // Сохраняем все в базу.
            return back()->with('message','Заказ отправлен');
        } else {
            $errorMsg = $validator->messages();
            return redirect()
            ->back()
            ->withErrors($errorMsg)
            ->withInput();
        }            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();
        return back()->with('message',"Заказ $id был удалён");
    }
}
