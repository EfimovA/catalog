<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Products extends Model
{
    protected $fillable = ['title', 'descriptions', 'preview', 'meta_description', 'meta_keywords', 'category_id','public'];


	public static function parameters($id)
	{
	    return DB::table('products')->where('products.id','=',$id)
	        ->join('parameters_values', 'parameters_values.products_id', '=', 'products.id')
	        ->join('parameters', 'parameters_values.parameters_id', '=', 'parameters.id')
	        ->select('parameters.id','parameters.title', 'parameters_values.value', 'parameters.unit', 'products.preview')->get();
	}

	public static function del($id)
	{
		return DB::table('parameters_values')->where('products_id','=',$id)
		->join('parameters','parameters_values.parameters_id','=','parameters.id')
		->delete();
	}

	public function categories()
	{
		return $this->belongsToMany('App\Category', 'product_category', 'product_id', 'category_id');
	}

	public static function boot()
	{
		parent::boot();

		self::deleting(function($product)
		{
			$product->categories()->detach();
		});
	}

	public function order() 
	{
		return $this->belongsToMany('App\Order');
	}

	public function scopeSearchByKeyword($query, $keyword)
	{
		if ($keyword!='') {
			$query->where(function ($query) use ($keyword) {
				$query->where("title", "LIKE","%$keyword%")
				->orWhere("description", "LIKE", "%$keyword%")
				->orWhere("meta_description", "LIKE", "%$keyword%")
				->orWhere("meta_keywords", "LIKE", "%$keyword%");
			});
		}
		return $query;
	}	
}
