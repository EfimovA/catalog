<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameters_values extends Model
{		
    protected $fillable=['products_id','parameters_id','value'];
}
