@extends('layouts.main')
@section('title', 'Контакты СибНПК ТрансМашСтрой')
@section('description', '')
@section('keywords', "")
@section('page-header')
Контакты
@overwrite
@section('breadcrump')
<li class="active">Контакты</li>
@stop
@section('content')
<div class="col-md-12">
	<p class="lead">
		Общество с ограниченной ответственностью «Сибирский научно-промышленный концерн транспортного машиностроения и строительства»
		<strong>(ООО «СибНПК «ТрансМашСтрой»)</strong>		
	</p>
	<label for="addres">Центральный офис:</label>
	<dl class="dl-horizontal">
		<dt>Адрес:</dt>
		<dd>644005, Омская обл., Омск г., Карбышева ул., дом 1</dd>
		<dt>ИНН/КПП</dt>
		<dd>5501259437 /550501001</dd>
		<dt>ОГРН</dt>
		<dd>1145543033570</dd>
	</dl>
	<label for="requisits">Банковские реквизиты:</label>
	<dl class="dl-horizontal">
		<dt>р/сч</dt>
		<dd>40702-810-9-1343-0008919</dd>
		<dt>в Филиале</dt>
		<dd>№5440 ВТБ24 (ЗАО)</dd>
		<dt>к/с</dt>
		<dd>30101-810-4-5004-0000751</dd>
		<dt>БИК</dt>
		<dd>045004751</dd>
	</dl>
	<strong>Телефон:</strong>
	<a href="tel:(3812) 44-66-26">(3812) 44-66-26</a>        
</div>
<div class="col-md-12">
	<div class="col-md-4">
		<br>
		<label for="">Генеральный директор</label>
		<div class="thumbnail" style="height: 400px">
			<img src="..." alt="...">
			<div class="caption">
				<h3>Михайлов Михаил Николаевич</h3>
				<h4><strong>Тел. </strong><a href="tel:+7-909-53-57-222">+7-909-53-57-222</a></h4>
				<h4><strong>E-mail: </strong><a href="mailto:mihailov@sibnpk.com">mihailov@sibnpk.com</a></h4>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<br>
		<label for="">Калининградский филиал</label>
		<div class="thumbnail" style="height: 400px">
			<img src="..." alt="...">
			<div class="caption">
				<h3>Шевелев Николай Владимирович</h3>
				<h4><strong>Тел. </strong><a href="tel:+7-909-799-11-99">+7-909-799-11-99</a></h4>
			</div>
		</div>
	</div>		
</div>
@stop