@extends('layouts.main')
@section('title', 'Официальные документы СибНПК ТрансМашСтрой')
@section('description', '')
@section('keywords', "")
@section('page-header')
Официальные документы
@overwrite
@section('breadcrump')
<li class="active">Официальные документы</li>
@stop
@section('content')
<div class="col-md-12">
	<div class="col-md-4">
		<div class="thumbnail" style="min-height: 400px">
			<a href="images/sert.jpg"><img src="images/sert.jpg" alt="..."></a>
		</div>
	</div>
	<div class="col-md-4">
		<div class="thumbnail" style="min-height: 400px">
			<a href="images/Письмо-1.jpg"><img src="images/Письмо-1.jpg" alt="..."></a>
		</div>
	</div>		
		<div class="col-md-4">
		<div class="thumbnail" style="min-height: 400px;">
			<a href="images/Свидетельство-СРО.jpg"><img src="images/Свидетельство-СРО.jpg" alt="..."></a>
		</div>
	</div>
</div>
@stop