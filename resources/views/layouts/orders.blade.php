@extends('layouts.main')
@section('title', "Заказать $product->title")
@section('description', "$product->meta_description")
@section('keywords', "заказать $product->title, купить $product->title, цена $product->title")
@section('page-header')
{{"Заказать - уточнить цену $product->title"}}
@overwrite
@section('breadcrump')
<li><a href="{{url('/categories')}}">Наши продукты</a></li> 
@foreach($product->categories as $category)
<li><a href="{{url('categories')}}/{{$category->id}}">{{$category->name}}</a></li>
@endforeach
<li><a href="{{url('categories/products/show')}}/{{$product->id}}">{{$product->title}}</a></li>
<li class="active">Заказать {{$product->title}}</li>
@stop
@section('content')
<div class="col-md-8">
  <h2>Оставьте нам свои контактные данные и мы свяжемся с вами в ближайшее время</h2>
</div>
<div class="col-md-12">
 <h2>Ваш заказ</h2>
 <table width=100% class="table-responsive table-striped">
   <thead>
     <tr>
      <th>Идентификатор</th>
      <th>Изображение</th>
      <th>Название</th>
    </tr>
  </thead>
  <tr>
    <td>{{$product->id}}</td>
    <td><img height=50 src="{{explode(';',$product->preview)[0]}}"></td>
    <td>{{$product->title}}</td>
  </tr>
</table> 
<hr>       
</div>

<div class="col-md-12">
  <h2>Заполните свои контактные данные</h2>
    @if(Session::has('message'))
    <div class="alert alert-success" role="alert">
      {{Session::get('message')}}
    </div>
    @endif
  <form method="POST">
    <label for="full_name">Ваше имя</label><br>
    <div class="{{ $errors->has('full_name') ? ' has-error' : '' }}">
      <input class="form-control" type="text" name="full_name" value="@if(!empty(old('full_name'))){{ old('full_name') }}@endif"/><br>
      @if ($errors->has('full_name'))
      <span class="help-block">
          <strong>{{ $errors->first('full_name') }}</strong>
      </span>
      @endif 
    </div>   
    <label  for="email">Ваша электронная почта</label><br>
    <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
      <input class="form-control" type="text" name="email" value="@if(!empty(old('email'))){{ old('email') }}@endif"/><br>
      @if ($errors->has('email'))
      <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
      </span>
      @endif    
    </div> 
    <label for="phone">Телефон</label><br>
    <div class="{{ $errors->has('phone') ? ' has-error' : '' }}">
      <input class="form-control" id="phone" type="text" name="phone" placeholder="+7 (999) 999 99 99" value="@if(!empty(old('phone'))){{ old('phone') }}@endif"/><br>
      @if ($errors->has('phone'))
      <span class="help-block">
          <strong>{{ $errors->first('phone') }}</strong>
      </span>
      @endif     
    </div>
    <label for="additional_info">Дополнительная информация</label><br>
    <div class="{{ $errors->has('additional_info') ? ' has-error' : '' }}">
      <textarea class="form-control" type="text" name="additional_info" rows="5">@if(!empty(old('additional_info'))){{ old('additional_info') }}@endif</textarea>
      @if ($errors->has('additional_info'))
      <span class="help-block">
          <strong>{{ $errors->first('additional_info') }}</strong>
      </span>
      @endif   
    </div>  
    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
    <br>
    {!! app('captcha')->display(); !!}
    <br>
    <button type="submit" class="btn btn-primary btn-lg" name="product_id" value="{{$product->id}}">Заказать</button>
  </form>
  <br>        
</div>
@stop