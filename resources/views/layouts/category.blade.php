@extends('layouts.main')
@section('title', 'Наши продукты')
@section('description', 'Каталог продукции СибНПК')
@section('keywords', "$forKeywords")
@section('page-header')
Наши продукты
@overwrite
@section('breadcrump')
<li class="active">Наши продукты</li>
@stop
@section('content')
<div class="row">
    <div class="same-height col-md-12">
        {!!$categoriesRecursive!!}
    </div>
</div>
@stop