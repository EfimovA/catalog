@extends('layouts.main')
@section('title', "$products->title")
@section('description', "$products->meta_description")
@if(!empty($products->meta_keywords))
@section('keywords', "$products->meta_keywords")
@else
@section('keywords', "$forKeywords")
@endif    
@section('page-header')
{{$products->title}}
@overwrite
@section('breadcrump')
<li><a href="{{url('/categories')}}">Наши продукты</a></li> 
@foreach($products->categories as $category)
<li><a href="{{url('categories')}}/{{$category->id}}">{{$category->name}}</a></li>
@endforeach
<li class="active">{{$products->title}}</li>
@stop
@section('content')
<div class="col-md-12">
  <div class="row">
    <div class="col-md-12">
      @foreach($images as $image)
      <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="column">
          <img src="{{$image}}" style="width:290px; height:190px; border-radius: 10px" onclick="openModal();currentSlide({{$loop->iteration}})" class="hover-shadow">
        </div>            
      </div>
      @endforeach          
    </div>

  </div>

  <div id="myModal" class="modal">
    <span class="close cursor" onclick="closeModal()">&times;</span>
    <div class="modal-content">

      @foreach($images as $image)
      <div class="mySlides">
        <div class="numbertext">{{$loop->iteration}} / {{$loop->count}}</div>
        <img src="{{$image}}" style="width:100%">
      </div>
      @endforeach

      <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
      <a class="next" onclick="plusSlides(1)">&#10095;</a>

      <div class="caption-container">
        <p id="caption"></p>
      </div>

      @foreach($images as $image)
      <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="column">
          <img class="demo" src="{{$image}}" style="width:290px; height:190px" onclick="currentSlide({{$loop->iteration}})" alt="{{$products->title}}">
        </div>
      </div>
      @endforeach

    </div>
  </div>
</div>
<div class="col-md-12">
  <h3>{!!$products->title!!}</h3>
  <div class="panel panel-default">
    <div class="panel-heading">Описание</div>
    <div class="panel-body">
      {!!$products->description!!}
    </div>
  </div>
  <h3>Параметры</h3>
  <table class="table table-striped">
    <thead>
      <th>Название</th>
      <th>Значение</th>
      <th>Ед. измерения</th>
    </thead>
    <tbody>
      @foreach($parameters as $parameter)
      <tr>
        <td>{{$parameter->title}}</td>
        <td>{{$parameter->value}}</td>
        <td>{{$parameter->unit}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
<div class="col-md-12">
  <form action="/orders/{{$id}}">
    <button class="btn btn-primary btn-lg" type="submit">Заказать</button>
    <button class="btn btn-default btn-lg" type="submit">Уточнить цену</button>  
  </form> 
  <br>     
</div>
  


<script>
  function openModal() {
    document.getElementById('myModal').style.display = "block";
    document.getElementById('nav').style.display = "none";
  }

  function closeModal() {
    document.getElementById('myModal').style.display = "none";
    document.getElementById('nav').style.display = "block";
  }

  var slideIndex = 1;
  showSlides(slideIndex);

  function plusSlides(n) {
    showSlides(slideIndex += n);
  }

  function currentSlide(n) {
    showSlides(slideIndex = n);
  }

  function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    var captionText = document.getElementById("caption");
    if (n > slides.length) {slideIndex = 1}
      if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
          slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
          dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        captionText.innerHTML = dots[slideIndex-1].alt;
      }
    </script>
    @stop