@extends('layouts.main')
@section('title', "$category->name")
@section('description', "$category->name")
@section('keywords', "$forKeywords")
@section('page-header')
{{$category->name}}
@overwrite
@section('breadcrump')
  <li><a href="{{url('/categories')}}">Наши продукты</a></li> 
  @if($category->isChild())
  	@foreach($category->getAncestors() as $ancestor)
		<li><a href="{{url('categories')}}/{{$ancestor->id}}">{{$ancestor->name}}</a></li>
  	@endforeach
  @endif 
  <li class="active">{{$category->name}}</li>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
		@foreach($products as $product)
			<div class="col-xs-12 col-sm-4 col-lg-4 col-md-4">
				<div class="thumbnail">
					<a href="products/show/{{$product->id}}"><img src="{{explode(';',$product->preview)[0]}}" id="main_image" alt=""></a>
					<div class="caption">
						<h3>{{$product->title}}</h3>
						<p><a href="/orders/{{$product->id}}" class="btn btn-default" id="{{$product->id}}" role="button">Уточнить цену</a> 
						<a  href="/orders/{{$product->id}}"><button class="btn btn-primary" id="{{$product->id}}" role="button">Заказать</button></a>
						<a href="products/show/{{$product->id}}" class="btn btn-default" id="{{$product->id}}"role="button">Подробнее</a></p>
					</div>
				</div>
			</div>
		@endforeach
		<div class="col-xs-offset-5 col-sm-offset-5 col-md-offset-5">{{ $products->links() }}</div>		
	</div>
</div>
<div class="row">
    <div class="col-md-12">
		{!!$categoriesRecursive!!}
    </div>    
</div>
@stop