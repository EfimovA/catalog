<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" value="@yield('description', 'Сибирский научно-промышленный концерн транспортного машиностроения и строительства (СибНПК «ТрансМашСтрой») — многопрофильное предприятие, оказывающее услуги по производству оборудования и металлообработке как частным лицам, так и предприятиям')">
        <meta name="keywords" value="@yield('keywords','СибНПК, ТрансМашСтрой, ТрансМаш, Сибирский научно-производственный концерн Транспортного Машиностроения, каталог продукции СибНПК, СибНПК ТрансМашСтрой')">

        <title>@yield('title', 'Сибирский научно-производственный концерн Транспортного Машиностроения')</title>

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="{{asset("css/bootstrap.min.css")}}"/>

                <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="{{asset("vendor/metisMenu/metisMenu.min.css")}}"/>

        <!-- Custom CSS -->
        <link href="{{asset("css/shop-homepage.css")}}" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="{{asset("vendor/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <!-- Navigation -->
        @include('partials.navigation')
        @yield('navigation')

        <div class="container">
            <div class="row row-box-shadow">
            
                <div class="page-header col-md-12">
                  <h1>@yield('page-header')</h1>                  
                </div>

                <div class="col-md-12">
                    <ol class="breadcrumb">
                      <li><a href="{{url('/')}}">Главная</a></li>
                      @yield('breadcrump')
                    </ol>                    
                </div>                 


            <!-- Page Header -->
            @include('partials.sidebar')
            @yield('sidebar')

            <!-- Main Content -->
            @yield('content')

            </div>
        </div>

        <!-- Footer -->
        @include('partials.footer')
        @yield('footer')

        <!-- jQuery -->
        <script src="{{asset("js/jquery-2.1.4.min.js")}}"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{asset('js/bootstrap.min.js')}}"></script>

        <script src="{{asset('js/functions.js')}}"></script> 

        <script src="{{asset('js/jquery.cookie.js')}}"></script>  

        <script src="{{asset('vendor/metisMenu/metisMenu.min.js')}}"></script>

    </body>

</html>
