@extends('layouts.main')
@section('title', "Поиск по запросу $keyword")
@section('description', "")
@section('keywords', "")
@section('page-header')
Поиск по запросу "{{$keyword}}"
@overwrite
@section('breadcrump')
  <li class="active">Поиск "{{$keyword}}"</li>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
		@foreach($productsInSearch as $product)
			<div class="col-xs-12 col-sm-4 col-lg-4 col-md-4">
				<div class="thumbnail">
					<a href="categories/products/show/{{$product->id}}"><img src="{{explode(';',$product->preview)[0]}}" id="main_image" alt=""></a>
					<div class="caption">
						<h3>{{$product->title}}</h3>
						<p><a href="/orders/{{$product->id}}" class="btn btn-default" id="{{$product->id}}" role="button">Уточнить цену</a> 
						<a  href="/orders/{{$product->id}}"><button class="btn btn-primary" id="{{$product->id}}" role="button">Заказать</button></a>
						<a href="categories/products/show/{{$product->id}}" class="btn btn-default" id="{{$product->id}}"role="button">Подробнее</a></p>
					</div>
				</div>
			</div>
		@endforeach
		<div class="col-xs-offset-5 col-sm-offset-5 col-md-offset-5">{{ $productsInSearch->links() }}</div>		
	</div>
</div>
<div class="row">
    <div class="col-md-12">
    	<ul>
			@foreach($categoriesInSearch as $category)
				<li>
					<a href="/categories/{{$category->id}}">
						<h2>{{$category->name}}</h2>
					</a>					
				</li>			
			@endforeach  
			<div class="col-xs-offset-5 col-sm-offset-5 col-md-offset-5">{{ $categoriesInSearch->links() }}</div>	    	
    	</ul>    	
    </div>    
</div>
@stop