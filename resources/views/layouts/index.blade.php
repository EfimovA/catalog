@extends('layouts.main')
@section('page-header')
Добро пожаловать
@overwrite
@section('content')
<div class="col-md-8">

	<div class="row carousel-holder">

		<div class="col-md-12">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="item active">
						<img class="slide-image" src="images\Selhoz.jpg" alt="">
					</div>
					<div class="item">
						<img class="slide-image" src="images\Котельное-оборудование-960x399.jpg" alt="">
					</div>
					<div class="item">
						<img class="slide-image" src="images\Proektnaya-organizatsiya-960x399.jpg" alt="">
					</div>
				</div>
				<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</div>

	</div>
</div>

<div class="col-md-12">
	<div class="jumbotron col-md-8 col-md-offset-4">
		<h1><strong>О компании:</strong></h1>
		<div class="row">
			<div class="col-md-5">
				<img class="img-rounded" src="images\bigceh_resize.jpg" alt="">
			</div>
			<div>Сибирский научно-промышленный концерн транспортного машиностроения и строительства (<strong>СибНПК «ТрансМашСтрой»</strong>) — многопрофильное предприятие, оказывающее услуги по производству оборудования и металлообработке как частным лицам, так и предприятиям
				<p>Для осуществления комплексных решений «ПОД КЛЮЧ» мы объединили в себе такие направления деятельности, как:</p>                                            
				<ul>
					<li>проведение технико-технологических аудитов предприятий;</li>
					<li>проектирование технологических решений оснащения предприятий;</li>
					<li>проектирование объектов капитального строительства и реконструкции;</li>
					<li>строительство и реконструкция зданий и сооружений;</li>
					<li>разработка и изготовление нестандартных средств технологического оснащения предприятий;</li>
					<li>сервисное гарантийное и постгарантийное обслуживание стандартного (станочного) и нестандартного оборудования.</li>
				</ul>
				<p>Наши технологические и строительные решения успешно применяются на различных объектах.
					ООО «СибНПК «ТрансМашСтрой» предлагает продукцию
					<strong>собственного производства:</strong></p>
					<ul>
						<li>сельскохозяйственного назначения;</li>
						<li>автосервисное оборудование;</li>
						<li>мебель из металла;</li>
						<li>металлические изделия для дома и дачи;</li>
						<li>автономные быстровозводимые здания;</li>
						<li>мобильные здания.</li>                                          
					</ul>
					<p>Если Вы не нашли в нашем каталоге интересующее Вас технологическое оборудование или оно не подходит по каким-либо параметрам, сделайте запрос в наш адрес. Мы являемся разработчиком и изготовителем нестандартного оборудования и наверняка сможем Вам помочь</p>
					<p>Обращаем Ваше внимание, что на некоторые образцы нашей продукции мы не имеем возможности указать конкретную цену, так как процесс ее формирования дифференцирован и зависит от множества факторов, изменяющихся во времени и в соответствии с условиями изготовления и поставки.</p>
						<p><strong>Надежность и безопасность начинается с качества!</strong></p>
						<p>С уважением, коллектив ООО «СибНПК «ТрансМашСтрой»</p>
					</div>                                     
				</div>                          
			</div>    
		</div>

		@stop