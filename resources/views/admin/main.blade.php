<!DOCTYPE html>
<html>
    <head>
        <meta charaset="utf-8"/>
        <title>Админка</title>
        <script src="{{asset("js/jquery-2.1.4.min.js")}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <link rel="stylesheet" href="{{asset('css/admin.css')}}">
        <link rel="stylesheet" href="{{asset("css/bootstrap.min.css")}}"/>
        <link rel="stylesheet" href="{{asset("css/bootstrap-theme.min.css")}}"/>
        <link href="{{asset("css//select2.min.css")}}" rel="stylesheet" />
        <script src="{{asset("js/functions.js")}}"></script>
        <script src="{{asset('js/tinymce/tinymce.min.js')}}"></script>
        <script src="{{asset('js/select2.min.js')}}"></script>
        <script src="{{asset('vendor/metisMenu/metisMenu.min.js')}}"></script>
        <script>
        tinymce.init({            
            selector: '#editor',
            file_browser_callback : elFinderBrowser,
            language: 'ru',
            // height: 500,
            // theme: 'modern',
            // plugins: [
            // 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            // 'searchreplace wordcount visualblocks visualchars code fullscreen',
            // 'insertdatetime media nonbreaking save table contextmenu directionality',
            // 'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
            // ],
            // toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            // toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            // image_advtab: true,
            // templates: [
            // { title: 'Test template 1', content: 'Test 1' },
            // { title: 'Test template 2', content: 'Test 2' }
            // ],
            // content_css: [
            // '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            // '//www.tinymce.com/css/codepen.min.css'
            // ]

            plugins: [
            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | visualchars visualblocks nonbreaking template pagebreak restoredraft",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [{
            title: 'Bold text',
            inline: 'b'
            }, {
            title: 'Red text',
            inline: 'span',
            styles: {
              color: '#ff0000'
            }
            }, {
            title: 'Red header',
            block: 'h1',
            styles: {
              color: '#ff0000'
            }
            }, {
            title: 'Example 1',
            inline: 'span',
            classes: 'example1'
            }, {
            title: 'Example 2',
            inline: 'span',
            classes: 'example2'
            }, {
            title: 'Table styles'
            }, {
            title: 'Table row 1',
            selector: 'tr',
            classes: 'tablerow1'
            }],

            templates: [{
            title: 'Test template 1',
            content: 'Test 1'
            }, {
            title: 'Test template 2',
            content: 'Test 2'
            }],
            content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
            ]
        });
        function elFinderBrowser (field_name, url, type, win) {
            tinymce.activeEditor.windowManager.open({
                file: '/adminzone/elfinder',// use an absolute path!
                title: 'elFinder 2.0',
                width: 900,
                height: 450,
                resizable: 'yes'
            }, {
                setUrl: function (url) {
                    win.document.getElementById(field_name).value = url;
                }
            });
            return false;
        }
        </script>
    </head>

    <nav class="navbar navbar-default">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Админка</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="/adminzone/categories">Категории<span class="sr-only">(current)</span></a></li>
            <li><a href="/adminzone/products">Продукты</a></li>
            <li><a href="/adminzone/orders">Заказы<span class="badge">{{App\Order::where('viewed', '=', 'false')->count()}}</span></a></li>
            <li><a href="/adminzone/pages">Страницы</a></li>
            <li><a href="/adminzone/elfinder">Медиа</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}} <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li>
                    <a href="{{ url('/logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Выйти
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>                    
                </li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="content">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>


