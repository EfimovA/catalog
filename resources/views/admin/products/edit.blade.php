@extends('admin.main')
@section('content')
<!DOCTYPE html>
<html>
<head>
  <title>Изменение товара</title>
  <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
  @if(Session::has('message'))
  <div class="alert alert-success" role="alert">
    {{Session::get('message')}}
  </div>
  @endif
  @if (count($errors) > 0)
  <ul>
    @foreach ($errors->all() as $error)
    <div class="alert alert-warning" role="alert"><li>{{ $error }}</li></div>
    @endforeach
  </ul>
  @endif
  <input type="hidden" id="product_id" value="{{$product->id}}">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="/adminzone">Назад</a> <span class="divider"></span></li> 
      <li><a href="/adminzone/products">Все товары</a> <span class="divider"></span></li>              
      <li class="active">Редактирование товара</li>
    </ul>
    <h1>Изменение товара</h1>
    <hr>
    <form method="POST" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{csrf_token()}}"/>
      <div class="row">
        @if(!empty($images)) <!-- проверяем если картинки -->
        @foreach($images as $image)
        <div class="img-thumbnail">
          <img width=100 src="{{$image}}">
          <button type="button" title="Удалить" class="btn btn-danger del_image btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
        </div>
        @endforeach
        @endif
      </div>
      <hr>
      <div class="row">
        <div class="col-md-4">
          <input type="file" name="preview[]"><br>
        </div>
        <div class="col-md-8">
          <i class="glyphicon glyphicon-arrow-left"></i> Выберите миниатюру для товара. <p class="help-block">Размер изображения 150x150px, не более 200Кб</p>
        </div>
      </div>
      <h3>Дополнительные изображения</h3><button class="btn btn-primary add_images" type="button"><i class="glyphicon glyphicon-plus"></i></button>
      <hr>
      <div class="row">
        <div class="col-md-8">
          <label for="category" class="control-label">Категория <span style="color:red">*</span></label><br>
          <div class="{{ $errors->has('category_id') ? ' has-error' : '' }}">
            <select class="form-control multiselect" id="category_id" name="category_id[]" data-placeholder="Please select category" multiple="multiple">
              @if(isset($category_id))
                @foreach($category_id as $key => $category)
                <option value="{{$key}}" selected>{{$category}}</option>
                @endforeach
              @elseif(old('category_id')!=null)
                @foreach($category_id=old('category_id') as $keyOld => $categoryOldId)
                  @foreach($categories as $key => $categoryName)
                    @if($key==$categoryOldId)        
                      <option value="{{$key}}" selected>{{$categoryName}}</option>
                    @endif
                  @endforeach
                @endforeach
              @elseif(old('category_id')==null)
                @foreach($product->categories as $category)
                  <option value="{{$category->id}}" selected>{{$category->name}}</option>
                @endforeach
              @endif
              @foreach($categories as $key => $category)
                <option value="{{$key}}">{{$category}}</option>
              @endforeach
            </select>
            @if ($errors->has('category_id'))
            <span class="help-block">
              <strong>{{ $errors->first('category_id') }}</strong>
            </span>
            @endif
          </div>
          <label class="control-label" for="name">Название товара <span style="color:red">*</span></label>
          <div class="{{ $errors->has('title') ? ' has-error' : '' }}">
          <input class="form-control" type="text" name="title" value="@if(!empty(old('title'))){{ old('title') }}@else{{$product->title}}@endif">
          @if ($errors->has('title'))
          <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
          </span>
          @endif
          </div>
          <label for="description" class="control-label">Описание товара <span style="color:red">*</span></label>
          <textarea class="form-control" rows="4" name="description" id="editor">@if(!empty(old('description'))){{ old('description') }}@else{{$product->description}}@endif</textarea>
          @if ($errors->has('description'))
          <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
          </span>
          @endif
          <div class="row">
            <div class="col-md-12">                        
              Опубликовать?<br>
              <select name="public">
                <option value="1">Да</option>
                <option value="0">Нет</option>
              </select>   
              <h3>Мета</h3>            
              <div class="row">
                <div class="col-md-8">
                  <label for="meta_description" class="control-label">description <span style="color:red">*</span></label>
                  <div class="{{ $errors->has('meta_description') ? ' has-error' : '' }}">
                  <textarea class="form-control" type="text" name="meta_description" rows="3">@if(!empty(old('meta_description'))){{ old('meta_description') }}@else{{$product->meta_description}}@endif</textarea>
                  @if ($errors->has('meta_description'))
                  <span class="help-block">
                    <strong>{{ $errors->first('meta_description') }}</strong>
                  </span>
                  @endif
                  </div>
                  keywords:<br>
                  <input class="form-control" type="text" name="meta_keywords" value="{{$product->meta_keywords}}"/>
                </div>
                <div class="col-md-4"> 
                  <i class="glyphicon glyphicon-arrow-left"></i> Заполните description кратким описанием товара. 
                  <p class="help-block">В поле keywords введите теги товара через запятую</p> 
                </div>                        
              </div>
            </div>
          </div>
          <h3>Характеристики товара</h3>
          <button class="btn btn-primary btn-lg add_button" type="button">Добавить</button>
          <br><br>
          <div>
            @if(!empty(old('parameter')))
            @foreach($parametersOld=old('parameter') as $keyParam => $parameterOld)
            <div class="form-inline borderes" role="form">
                <div class="form-group">
                    <label for="parameter" class="sr-only">Параметр</label>
                    <div class="input-group">
                        <span class="input-group-btn">
                        <button class="btn btn-default  add_parameter" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                        </span>
                        <select class="form-control" name="parameter[]">
                            @foreach($parameters as $parameter)
                            @if($parameter->id==$parameterOld)
                            <option value="{{$parameterOld}}" selected>{{$parameter->title}} ({{$parameter->unit}})</option>
                            @else
                            <option value="{{$parameter->id}}">{{$parameter->title}} ({{$parameter->unit}})</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="value" class="sr-only">Значение параметра</label>
                        @if(!empty(old('value')))
                        @foreach($valuesOlds=old('value') as $keyVal => $valueOld)
                        @if($keyVal==$keyParam)
                        <input class="form-control" name="value[]" placeholder="Значение параметра" 
                        value="{{$valueOld}}"/>
                        @endif
                        @endforeach                        
                        @endif
                    </div>
                    <div class="form-group">
                        <span class="form-group-btn">
                        <button class="btn btn-default remove_button"><i class="glyphicon glyphicon-minus"></i></button>
                        </span>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            @foreach($parameters as $param)
            <div class="form-inline">
             <div class="form-group">
               <label for="parameter" class="sr-only">Параметр</label>
               <div class="input-group">
                <span class="input-group-btn">
                  <button class="btn btn-default  add_parameter" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                </span>
                <select class="form-control" name="parameter[]">
                  @foreach($parameters_all as $parameter)
                  @if($param->id==$parameter->id)
                  <option value="{{$parameter->id}}" selected>{{$parameter->title}} ({{$parameter->unit}})</option>
                  @else
                  <option value="{{$parameter->id}}">{{$parameter->title}} ({{$parameter->unit}})</option>
                  @endif
                  @endforeach
                </select>
              </div>
              <div class="form-group">
               <label for="value" class="sr-only">Значение параметра</label>
               <input class="form-control" name="value[]" value="{{$param->value}}"/>
             </div>
             <div class="form-group">
             <button class="btn btn-default remove_button" type="button"><i class="glyphicon glyphicon-minus"></i></button>
             </div>
           </div>
         </div>
         @endforeach
         @endif
       </div>
       <br>
       <button type="submit" class="btn btn-default btn-lg save_product">Сохранить товар</button>
     </div>
   </div>
 </form>
</div>

</body>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Добавить параметр</h4>
      </div>
      <div class="modal-body">
        <input type="text" class="form-control parameter_modal" name="parameter" placeholder="Наименование параметра"><br>
        <input type="text" class="form-control unit_modal" name="unit" placeholder="Единица измерения">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary save_and_close">Сохранить изменения</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal -->

</html>
<script type="text/javascript">
  $(function() {
    $(".multiselect").select2();
  });
</script>
@endsection
