@extends('admin.main')
@section('content')
<ul class="breadcrumb">          
  <li class="active">Все товары</li>
</ul>

<div class="panel panel-default">
    <div class="panel-heading">Продукты&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="{{ route('products.create') }}" class="btn btn-xs btn-success">&nbsp;&nbsp;Создать&nbsp;&nbsp;</a>
    </div>
    <div class="panel-body">
        <div class="col-xs-offset-8 col-xs-4" style="margin-bottom: 15px;">
            <form id="filter" action="{{route('products.index')}}" method="get">{!! csrf_field() !!}<select name="filter[]" data-placeholder="Фильтр категорий" class="form-control multiselect" multiple="multiple">
                {!! $options !!}
            </select></form>
        </div>
        <table class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th class="col-md-2">Изображение</th>
                    <th class="col-md-3">Название</th>                    
                    <th class="col-md-2">Категория(ии)</th>
                    <th class="col-md-2 text-center">Действия</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                <tr>
                    <?php 
                    $images=explode(';',$product->preview);
                    ?>
                    <td>
                        <img src="{{$images[0]}}" width="130" class="img-responsive img-rounded" alt="{{ $product->title }}">
                    </td>
                    <td>{{ $product->title}}</td>                    
                    <td>
                        @foreach($product->categories as $index => $category)
                        {{ $category->name }}@if($index+1 < count($product->categories)),@endif
                        @endforeach
                    </td>
                    <td class="text-center">
                        <form class="form-inline">
                            <a href="{{ route('products.edit', ['products' => $product->id]) }}" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="Изменить">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </a>
                            <a href="{{ route('products.destroy', ['products' => $product->id]) }}" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" data-delete="{{ csrf_token() }}" title="Удалить">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </a>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $products->links() }}
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('[data-toggle="tooltip"]').tooltip();

        $(".multiselect").select2();

        $('.multiselect').on("change", function() {
            var categories = $('.multiselect').val();
            console.log(categories);

            $("#filter").submit();
        });

        $('[data-delete]').click(function(e){
            e.preventDefault();
                // If the user confirm the delete
                if (confirm('Do you really want to delete this product?')) {
                    // Get the route URL
                    var url = $(this).prop('href');
                    // Get the token
                    var token = $(this).data('delete');
                    // Create a form element
                    var $form = $('<form/>', {action: url, method: 'post'});
                    // Add the DELETE hidden input method
                    var $inputMethod = $('<input/>', {type: 'hidden', name: '_method', value: 'delete'});
                    // Add the token hidden input
                    var $inputToken = $('<input/>', {type: 'hidden', name: '_token', value: token});
                    // Append the inputs to the form, hide the form, append the form to the <body>, SUBMIT !
                    $form.append($inputMethod, $inputToken).hide().appendTo('body').submit();
                }
            }       );
    });
</script>

@if(Session::has('message'))
{{Session::get('message')}}
@endif
@endsection