@extends('admin.main')
@section('content')
<!DOCTYPE html>
<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Добавить товар</title>          
    </head>
    <body>
        @if(Session::has('message'))
        <div class="alert alert-success" role="alert">
          {{Session::get('message')}}
        </div>
        @endif
        @if (count($errors) > 0)
        <ul>
          @foreach ($errors->all() as $error)
          <div class="alert alert-warning" role="alert"><li>{{ $error }}</li></div>
          @endforeach
        </ul>
        @endif
        <div class="container">
        <form id="product-form" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <ul class="breadcrumb">
              <li><a href="/adminzone/products">Назад</a> <span class="divider"></span></li>              
              <li class="active">Добавление товара</li>
            </ul>
            <h1>Добавить товар</h1>
            <hr>
            <div class="row">
                <div class="col-md-4{{ $errors->has('preview') ? ' has-error' : '' }}">
                    <input type="file" name="preview[]"/><br>
                    @if ($errors->has('preview'))
                    <span class="help-block">
                        <strong>{{ $errors->first('preview') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-md-8">
                    <i class="glyphicon glyphicon-arrow-left"></i> Выберите миниатюру для товара. 
                    <p class="help-block">Размер изображения 150x150px, не более 200Кб</p>
                </div>
                </div>
            <div class="row">
                <div class="col-md-8">
                <h2>Дополнительные изображения</h2>
                <button class="btn btn-primary add_images" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                <hr>
                </div>
            </div> 
            <div class="row">
                <div class="col-md-8"> 
                    <label for="category" class="control-label">Категория <span style="color:red">*</span></label><br>                    

<!--                     <div class="form-group">
                        <label for="parent" class="col-sm-2 control-label">Категория<span style="color:red">*</span></label>
                        <div class="col-sm-10{{ $errors->has('category_id') ? ' has-error' : '' }}"> -->
                            <div class="{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <select class="form-control multiselect" id="category_id" name="category_id[]" data-placeholder="Please select category" multiple="multiple">
                            @if(isset($category_id))
                                @foreach($category_id as $key => $category)
                                    <option value="{{$key}}" selected>{{$category}}</option>
                                @endforeach
                            @elseif(old('category_id')!=null)
                                @foreach($category_id=old('category_id') as $keyOld => $categoryOldId)
                                    @foreach($categories as $key => $categoryName)
                                        @if($key==$categoryOldId)        
                                            <option value="{{$key}}" selected>{{$categoryName}}</option>
                                        @endif
                                    @endforeach
                                @endforeach
                            @endif
                            @foreach($categories as $key => $category)
                            <option value="{{$key}}">{{$category}}</option>
                            @endforeach
                            </select>
<!--                         </div>
                    </div> -->
                    @if ($errors->has('category_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('category_id') }}</strong>
                    </span>
                    @endif
                    </div>
                    <label class="control-label" for="name">Название товара <span style="color:red">*</span></label>
                    <div class="{{ $errors->has('title') ? ' has-error' : '' }}">
                    <input class="form-control" type="text" name="title" value="@if(isset($product->title)){{ $product->title }}@else{{ old('title') }}@endif"/>
                    @if ($errors->has('title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                    </div>
                    <label for="description" class="control-label">Описание товара <span style="color:red">*</span></label>
                    <textarea class="form-control" rows="4" name="description" id="editor">@if(isset($product->description)){{ $product->description }}@else{{ old('description') }}@endif</textarea>   
                    Опубликовать?<br>
                    @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                    @endif
                    <select name="public">
                        <option value="1">Да</option>
                        <option value="0">Нет</option>
                    </select>   
                    <h3>Мета</h3>                
                    <div class="row">
                        <div class="col-md-8">
                            <label for="meta_description" class="control-label">description <span style="color:red">*</span></label>
                            <div class="{{ $errors->has('meta_description') ? ' has-error' : '' }}">                   
                            <textarea class="form-control" type="text" name="meta_description" rows="3">@if(isset($product->meta_description)){{ $product->meta_description }}@else{{ old('meta_description') }}@endif</textarea>
                            @if ($errors->has('meta_description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('meta_description') }}</strong>
                            </span>
                            @endif
                            </div>
                            keywords:<br>                            
                            <input class="form-control" type="text" name="meta_keywords" value="@if(isset($product->meta_keywords)){{ $product->meta_keywords }}@else{{ old('meta_keywords') }}@endif"/>                
                        </div>
                        <div class="col-md-4">
                        <i class="glyphicon glyphicon-arrow-left"></i> Заполните description кратким описанием товара. 
                        <p class="help-block">В поле keywords введите теги товара через запятую</p> 
                        </div>                        
                    </div>
                </div>
            </div>
            <h4>Характеристики товара</h4>
            <button class="btn btn-primary btn-lg add_button" type="button">Добавить</button><br><br>
            @if(!empty(old('parameter')))
            @foreach($parametersOld=old('parameter') as $keyParam => $parameterOld)
            <div class="form-inline borderes" role="form">
                <div class="form-group">
                    <label for="parameter" class="sr-only">Параметр</label>
                    <div class="input-group">
                        <span class="input-group-btn">
                        <button class="btn btn-default  add_parameter" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                        </span>
                        <select class="form-control" name="parameter[]">
                            @foreach($parameters as $parameter)
                            @if($parameter->id==$parameterOld)
                            <option value="{{$parameterOld}}" selected>{{$parameter->title}} ({{$parameter->unit}})</option>
                            @else
                            <option value="{{$parameter->id}}">{{$parameter->title}} ({{$parameter->unit}})</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="value" class="sr-only">Значение параметра</label>
                        @if(!empty(old('value')))
                        @foreach($valuesOlds=old('value') as $keyVal => $valueOld)
                        @if($keyVal==$keyParam)
                        <input class="form-control" name="value[]" placeholder="Значение параметра" 
                        value="{{$valueOld}}"/>
                        @endif
                        @endforeach                        
                        @endif
                    </div>
                    <div class="form-group">
                        <span class="form-group-btn">
                        <button class="btn btn-default remove_button"><i class="glyphicon glyphicon-minus"></i></button>
                        </span>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
            <br>
            <button class="btn btn-default btn-lg btn_save_item" type="submit">Сохранить товар</button>
            </form>
        </div>
    </body>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Добавить параметр</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control parameter_modal" name="parameter" placeholder="Наименование параметра"/><br>
                    <input type="text" class="form-control unit_modal" name="unit" placeholder="Единица измерения"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary save_and_close">Сохранить изменения</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $(".multiselect").select2();
        });
    </script>
</html>
@endsection