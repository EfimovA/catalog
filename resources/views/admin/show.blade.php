<html>
<head>
    <title>Товар {{$products->title}} </title>
    <script src="{{asset("js/jquery-2.1.4.min.js")}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset("css/bootstrap.min.css")}}"/>
    <link rel="stylesheet" href="{{asset("css/bootstrap-theme.min.css")}}"/>
    <script src="{{asset("js/functions.js")}}"></script>
    <script src="{{asset("js/jquery.cookie.js")}}"></script>
</head>
<body>
    <nav class="navbar  navbar-default">
       <div class="container">
           <a href="/" class="navbar-text">Магазин</a>
           <!-- корзина -->
           <a href="/basket" class="navbar-link navbar-text navbar-right">
            <span style="font-size:1.5em;" class="glyphicon glyphicon-shopping-cart basket"></span>
            <span class="badge pull-right count_order">0</span>
        </a>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @foreach($images as $image)
            <img class="img-thumbnail" width=100 src="{{$image}}">

            <div class="modal fade" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    @endforeach
</div>
</div>
<h3>{{$products->title}}</h3>
<div class="panel panel-default">
    <div class="panel-heading">Описание</div>
    <div class="panel-body">
        {{$products->description}}
    </div>
</div>
<h3>Параметры</h3>
<table class="table table-striped">
    <thead>
        <th>Название</th>
        <th>Значение</th>
        <th>Ед. измерения</th>
    </thead>
    <tbody>
        @foreach($parameters as $parameter)
        <tr>
            <td>{{$parameter->title}}</td>
            <td>{{$parameter->value}}</td>
            <td>{{$parameter->unit}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<h2 class="text-success">Цена: {{$products->price}} руб.</h2>
<hr>
<button class="btn btn-primary buy-btn btn-lg" id={{$id}} price={{$products->price}} >Купить сейчас</button>
<button class="btn btn-default buy-now-btn btn-lg" id={{$id}} price={{$products->price}} >Добавить в корзину</button>
</div>
</body>
<script type="text/javascript">
    count_order();
</script>
</html>

