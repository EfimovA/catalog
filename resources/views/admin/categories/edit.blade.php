@extends('admin.main')
@section('content')
<ul class="breadcrumb">
  <li><a href="/adminzone">Назад</a> <span class="divider"></span></li> 
  <li><a href="/adminzone/categories">Все категории</a> <span class="divider"></span></li>              
  <li class="active">Изменить категорию</li>
</ul>
@if(Session::has('message'))
<div class="alert alert-success" role="alert">
  {{Session::get('message')}}
</div>
@endif
@if (count($errors) > 0)
<ul>
  @foreach ($errors->all() as $error)
  <div class="alert alert-warning" role="alert"><li>{{ $error }}</li></div>
  @endforeach
</ul>
@endif
    <form method="POST" action="{{action('CategoriesController@update',['categories'=>$category->id])}}">
        <label for="name_category"><strong>Название категории</strong></label><br>
        <div class="{{ $errors->has('name') ? ' has-error' : '' }}">
        <input type="text" class="form-control" name="name" value="{{$category->name}}">
        @if ($errors->has('name'))
        <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
        </div>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <br>
        <button class="btn btn-default" type="submit">Сохранить</button>
    </form>
@endsection
