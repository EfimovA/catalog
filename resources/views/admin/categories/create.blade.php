@extends('admin.main')
@section('content')
<ul class="breadcrumb">
  <li><a href="/adminzone/categories">Назад</a> <span class="divider"></span></li>              
  <li class="active">Создать категорию</li>
</ul>
@if(Session::has('message'))
<div class="alert alert-success" role="alert">
  {{Session::get('message')}}
</div>
@endif
@if (count($errors) > 0)
<ul>
  @foreach ($errors->all() as $error)
  <div class="alert alert-warning" role="alert"><li>{{ $error }}</li></div>
  @endforeach
</ul>
@endif
<form method="POST" action="{{action('CategoriesController@store')}}"/>
    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
    <label for="name_category"><strong>Название категории</strong></label><br>
    <div class="{{ $errors->has('name') ? ' has-error' : '' }}">
    <input type="text" class="form-control" name="name" placeholder="Введите название категории">
    @if ($errors->has('name'))
    <span class="help-block">
        <strong>{{ $errors->first('name') }}</strong>
    </span>
    @endif
    </div>
    <label for=""><strong>Выберите родительскую категорию</strong></label><br>    
    <select class="form-control" name="parent_id">
    <option value="null" disable selected>новая</option>
      @foreach($categories as $key => $category)
        <option value="{{$key}}">{{$category}}</option>
      @endforeach
    </select><br>    
    <button class="btn btn-default" type="submit">Сохранить</button>    
</form>
@stop
