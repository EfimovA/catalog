@extends('admin.main')
@section('content')
<ul class="breadcrumb">           
  <li class="active">Все категории</li>
</ul>
@if(Session::has('message'))
<div class="alert alert-success" role="alert">
  {{Session::get('message')}}
</div>
@endif
@if (count($errors) > 0)
<ul>
  @foreach ($errors->all() as $error)
  <div class="alert alert-warning" role="alert"><li>{{ $error }}</li></div>
  @endforeach
</ul>
@endif
@if(Session::has('errorMsg'))
<div class="alert alert-warning" role="alert">
  {{Session::get('errorMsg')}}
</div>
@endif
<div class="panel panel-default">
    <div class="panel-heading">Категории&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ route('categories.create') }}" class="btn btn-xs btn-success" style="font-weight: bold;">&nbsp;&nbsp;Создать&nbsp;&nbsp;</a></div>
    <div class="panel-body">
        <div>
            <table class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <th class="col-md-10">Категории</th>
                        <th class="col-md-2 text-center">Действия</th>
                    </tr>
                </thead>
                <tbody>
                    {!! $tree !!}
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();

        $('[data-delete]').click(function(e){
            e.preventDefault();
            // If the user confirm the delete
            if (confirm('Вы действительно хотите удалить данную категорию?')) {
                // Get the route URL
                var url = $(this).prop('href');
                // Get the token
                var token = $(this).data('delete');
                // Create a form element
                var $form = $('<form/>', {action: url, method: 'post'});
                // Add the DELETE hidden input method
                var $inputMethod = $('<input/>', {type: 'hidden', name: '_method', value: 'delete'});
                // Add the token hidden input
                var $inputToken = $('<input/>', {type: 'hidden', name: '_token', value: token});
                // Append the inputs to the form, hide the form, append the form to the <body>, SUBMIT !
                $form.append($inputMethod, $inputToken).hide().appendTo('body').submit();
            }
        });
    });
</script>
@endsection
