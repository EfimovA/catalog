@extends('admin.main')
@section('content')
<ul class="breadcrumb">          
  <li class="active">Все заказы</li>
</ul>
@if(Session::has('message'))
<div class="alert alert-success" role="alert">
  {{Session::get('message')}}
</div>
@endif
@if (count($errors) > 0)
<ul>
  @foreach ($errors->all() as $error)
  <div class="alert alert-warning" role="alert"><li>{{ $error }}</li></div>
  @endforeach
</ul>
@endif
@if(Session::has('errorMsg'))
<div class="alert alert-warning" role="alert">
  {{Session::get('errorMsg')}}
</div>
@endif
<div class="panel panel-default">
    <div class="panel-heading">Заказы&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="panel-body">
        <table class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th class="col-md-1">Код заказа</th>
                    <th class="col-md-1">Код продукта</th>
                    <th class="col-md-1">Статус</th>
                    <th class="col-md-2">Изображение</th>
                    <th class="col-md-2">Имя отправителя</th>
                    <th class="col-md-2">Краткое описание</th>
                    <th class="col-md-2 text-center">Действия</th>
                </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>  
                    <td>{{ $order->product_id }}</td>
                    @if($order->viewed==false)
                    <td>Новый</td>
                    @else
                    <td>Прочитано</td>
                    @endif
                    <?php 
                    $images=explode(';',$order->product->preview);
                     ?>
                    <td>
                        <img src="{{$images[0]}}" width="130" class="img-responsive img-rounded" alt="{{ $order->product->title }}">
                    </td>
                    <td>{{ $order->full_name }}</td>
                    <td>{{ str_limit($order->additional_info,20) }}</td>
                    <td class="text-center">
                        <form class="form-inline">
                            <a href="{{ route('readOrder', ['id' => $order->id])}}" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="Читать">
                                <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                            </a>
                            <a href="{{ route('deleteOrder', ['id' => $order->id]) }}" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" data-delete="{{ csrf_token() }}" title="Удалить">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                            </a>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('[data-toggle="tooltip"]').tooltip();

        $(".multiselect").select2();

        $('.multiselect').on("change", function() {
            var categories = $('.multiselect').val();
            console.log(categories);

            $("#filter").submit();
        });

        $('[data-delete]').click(function(e){
            e.preventDefault();
                // If the user confirm the delete
                if (confirm('Вы действительно хотите удалить данный заказ?')) {
                    // Get the route URL
                    var url = $(this).prop('href');
                    // Get the token
                    var token = $(this).data('delete');
                    // Create a form element
                    var $form = $('<form/>', {action: url, method: 'post'});
                    // Add the DELETE hidden input method
                    var $inputMethod = $('<input/>', {type: 'hidden', name: '_method', value: 'delete'});
                    // Add the token hidden input
                    var $inputToken = $('<input/>', {type: 'hidden', name: '_token', value: token});
                    // Append the inputs to the form, hide the form, append the form to the <body>, SUBMIT !
                    $form.append($inputMethod, $inputToken).hide().appendTo('body').submit();
                }
            }       );
    });
</script>

@if(Session::has('message'))
{{Session::get('message')}}
@endif
@endsection