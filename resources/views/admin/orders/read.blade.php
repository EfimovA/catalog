@extends('admin.main')
@section('content')
<ul class="breadcrumb">          
    <li><a href="/adminzone/orders">Все заказы</a></li>
    <li class="active">Заказ {{$order->id}}</li>
</ul>

<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <?php 
                $image = explode(';', $order->product->preview);
                ?>                
                <img src="{{$image[0]}}" class="img-responsive img-rounded" alt="Responsive image">
                <label class="col-sm-2 control-label">Отправитель</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{$order->full_name}}</p>
                </div>
                <label class="col-sm-2 control-label">Продукт</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{$order->product->title}}</p>
                </div>                            
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <a href="mailto:#"><p class="form-control-static">{{$order->email}}</p></a>
                </div>
                <label class="col-sm-2 control-label">Phone</label>
                <div class="col-sm-10">
                    <a href="tel:{{$order->phone}}"><p class="form-control-static">{{$order->phone}}</p></a>
                </div> 
                <label class="col-sm-2 control-label">Дополнительная информация</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{$order->additional_info}}</p>
                </div>                                
            </div>
        </form>
    </div>
</div>
@if(Session::has('message'))
{{Session::get('message')}}
@endif
@endsection