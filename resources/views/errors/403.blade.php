@extends('admin.main')
@section('content')
<!DOCTYPE html>
<html>
<head>
    <title>Ошибка доступа!</title>          
</head>
<body>
    <div class="container">           
        <hr>
        <div class="row">
            <div class="col-md-4">
                <h1>Для доступа к административным функциям пройдите авторизацию!</h1>
                <hr>
                <a href="/home"><button class="btn btn-primary btn-lg add_button" type="button">Авторизация</button></a><br><br>
            </div>
        </div> 
    </div>
</body>
</html>
@endsection





