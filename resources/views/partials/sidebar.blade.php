@section('sidebar')
<div class="col-md-4">
	<label class="lead">Категории товаров</label>
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav">
			<ul class="nav" id="side-menu">
				{!!$categories!!}
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
</div>
@stop