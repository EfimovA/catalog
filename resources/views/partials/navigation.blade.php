@section('navigation')
<!-- Navigation -->
<nav id="nav" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{url('/')}}"><img src="{{asset("images\logo.png")}}" alt=""></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li>
					<a href="{{url('/')}}/categories">Наши продукты и услуги</a>
				</li>
				<li>
					<a href="{{url('/')}}/contacts">Контакты</a>
				</li>
				<li>
					<a href="{{url('/')}}/documents">Документы</a>
				</li>
			</ul>
		      <form method="post" action="{{route('search')}}" class="navbar-form navbar-right">
		      	{{ csrf_field() }}
		        <div class="form-group">
		          <input type="text" class="form-control" name="keyword" placeholder="Поиск">
		        </div>
		        <button type="submit" class="btn-sm btn-default glyphicon glyphicon-search"></button>
		      </form>			
<!-- 			<form class="navbar-search pull-right">
				<input type="text" class="search-query" placeholder="Поиск">
			</form> -->
		</div>
		<!-- /.navbar-collapse -->            
	</div>
	<!-- /.container -->
</nav>
@stop