@section('footer')
<!-- /.container -->

<div class="container-fluid contain-footer">

	<hr>

	<!-- Footer -->
	<footer>
		<div class="row">
			<div class="col-lg-12">
				<div class="col-md-4">
					<address>
					  <label><strong>СибНПК ТрансМашСтрой</strong></label>
						<p>
							Общество с ограниченной
							ответственностью «Сибирский 
							научно-промышленный концерн 
							транспортного машиностроения и 
							строительства»
							(ООО СибНПК «ТрансМашСтрой»)							
						</p>
					</address>						
				</div>	
				<div class="col-md-4">
					<address>
					  <strong>Телефон</strong><br>
					  <span class="glyphicon glyphicon-earphone"></span> <a href="tel:(3812) 98-69-08">(3812) 98-69-08</a> <br>
					  <span class="glyphicon glyphicon-phone"></span> <a href="tel:+7-909-53-57-222">+7-909-53-57-222</a> <br>
  					  <strong>Адрес:</strong> 
  					  г. Омск, ул. Карбышева, д.1<br>
					</address>	
					<address>
					  <strong>Почта</strong><br>
					  <span class="glyphicon glyphicon-envelope"></span><a href="mailto:#">info@sibnpk.com</a>
					</address>								
				</div>	
				<div class="col-md-4">
					<address>
					  <strong>Московский филиал</strong><br>
					  <span class="glyphicon glyphicon-phone"></span> <a href="tel:+7 (925) 507-34-75">+7 (925) 507-34-75</a>
					</address>	
					<address>
					  <strong>Калининградский филиал</strong><br>
					  <span class="glyphicon glyphicon-phone"></span> <a href="tel:+7 (909) 799-11-99">+7 (909) 799-11-99</a>
					</address>								
				</div>											
			</div>
		</div>
	</footer>

</div>
<!-- /.container -->
@stop