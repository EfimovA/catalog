$(document).ready(function() {
    $('.add_button').click(function() {
        var button;
        var list;
        button = $(this); // объект кнопка
        $.ajax({
            url: '/get_parameters',
            type: "POST",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function($list) {
                button.after($list);
            },
            error: function(msg) {
                console.log(msg);
            }
        });
    });
});

$(document).on('click', '.remove_button', function() {
    var block;
    if (confirm('Delete?')) {
        block = $(this).parent().parent().parent();
        block.remove();
    }
});

$(document).on('click', '.add_parameter', function(){
            $('#myModal').modal();
            $('.alert').remove();
            $('.parameter_modal').val("");
            $('.unit_modal').val("");
});

$(document).on('click', '.save_and_close', function(){
    var title;
    var unit;
    title = $('.parameter_modal').val();
    unit = $('.unit_modal').val();
    $('.alert').remove();

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
        url: '/save_parameters',
        type: 'POST',
        data: {
            title: title,
            unit: unit
        },
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(param) {
            $('.modal-body').prepend('<div class="alert alert-success"> Success! </div>');

            $('select').append($('<option>', {
                value: param[0],
                text: param[1] + ' (' + param[2] + ')'
            })); //добавляем к существующему списку новый параметр
            $('#myModal').modal('hide');
        },

        statusCode: {
            422: function (data) {
                $('.modal-body').prepend('<div class="alert alert-danger">');  
                $('.alert-danger').append(data.responseJSON.title);
                $('.alert-danger').append(data.responseJSON.unit);
            }
        }
    });
});

$(document).on('click', '.add_images', function(){
    all = $('input[name="preview[]"]');
    if (all.length == 11) return; //ограничим количество картинок 1 превью и 10 дополнительных картинок.
    field = $('input[name="preview[]"]:first').clone(); // клонируем поле preview
    $(this).after(field); //вставляем поле после кнопки
})

$(document).on('click', '.del_image', function(){
    var div=$(this).parent(); //div, который содержить и картинку и кнопку
    var src=$(this).prev().attr('src'); //ссылка на кратинку
    var product_id=$("#product_id").val(); //id товара
    $.ajax({
        url: '/del_image',
        method: 'POST',
        data: {
            src:src,
            product_id:product_id
        },
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(res){
            div.remove(); //если все прошло без ошибок то удаляем div
        },
        error: function(msg){
            console.log(msg);// если ошибка, то можно посмотреть в консоле
        }
    });
});

$(function() {
    $('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent();

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
});