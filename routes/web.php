<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix'=>'adminzone'], function()
    {
        Route::get('/', function()
        {
            return view('admin.dashboard');
        });
        Route::resource('products','ProductsController');    
        Route::resource('pages','PagesController');
        Route::resource('categories','CategoriesController');
        Route::post('products/create','ProductsController@save');
        Route::get('products/edit/{id}','ProductsController@edit');
        Route::post('products/{id}/edit','ProductsController@update');    
        Route::get('categories/edit/{id}','CategoriesController@edit');
        Route::post('categories/edit/{id}','CategoriesController@update');  
        Route::get('orders','OrdersController@index');
        Route::delete('orders/{id}','OrdersController@destroy')->name('deleteOrder');
        Route::get('orders/{id}','OrdersController@readOrder')->name('readOrder');


        Route::get('elfinder',function(){
            return view('admin.elfinder');
        });

        Route::get('connector','ElfinderController@connector');
        Route::post('connector','ElfinderController@connector');

    });
});



// Route::get('/', function()
// {
//     $products = App\Products::where('public', '=', 1)->get(); // получаем все, что касается товара (название, цена....)
//     $categories = App\Category::all();
//     $parameters = App\parameters::all();//получаем все параметры
//     $images = explode(';',$products[0]->preview); //ссылки на картинки передаем отдельным массивом
//     return view('layouts.index', ['products'=>$products, 'parameters'=>$parameters, 'images'=>$images, 'categories'=>$categories]);
// });

Route::get('/', 'ProductsController@indexFront');

Route::get('categories/products/show/{id}','FrontController@showProduct');


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::post('get_parameters','ParametersController@get');
Route::post('save_parameters','ParametersController@save');
Route::post('del_image','ProductsController@del_image');

Route::get('categories', 'FrontController@showCategories');
Route::get('categories/{id}', 'FrontController@showProductsOfCategory');
Route::get('contacts', 'FrontController@showContacts');
Route::get('documents', 'FrontController@showDocuments');

Route::get('/orders/{id}','OrdersController@create');
Route::post('/orders/{id}','OrdersController@store');
Route::match(['post','get'],'search','FrontController@search')->name('search');


